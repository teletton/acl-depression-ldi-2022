## redundant log removal (jobs already stored)
import glob
import os

def read_job_list(job_folder):
    """
    A method which outputs a list of current results
    """
    all_jobs = []
    for flx in glob.glob(job_folder):
        handle = open(flx, "r")
        actual_jobs = handle.read().split("\n")
        all_jobs.append((actual_jobs, flx))
        handle.close()        
    return all_jobs

def current_results(res_folder):
    """
    A method which outputs all results via glob package
    """
    all_results = glob.glob(res_folder)
    return all_results

def clean_jobs(results, jobs):
    """
    A method which identifies redundant jobs and flags them for removal (with subsequent removal)
    """
    flagged_files = []
    for job in jobs:
        for result in results:
            rid = result.split("/")[-1]
            for jid in job[0]:
                if rid in jid:
                    flagged_files.append(job[1])
    flagged_files = set(flagged_files)

    for filx in flagged_files:
        os.remove(filx)
    
if __name__ == "__main__":

    jobs_folder = "current_jobs/*"
    results_folder = "results/*"

    all_results = current_results(results_folder)
    all_jobs = read_job_list(jobs_folder)
    clean_jobs(all_results, all_jobs)
