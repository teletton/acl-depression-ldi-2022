## Gather result JSON files and make a nice table.
import pandas as pd
import pickle
import os
import tarfile
import json
import glob


def extract_key_rows(fname):

    with open(fname, "r") as jsonFile:
        try:
            jsonObject = json.loads(jsonFile.read())

        except Exception as es:
            print(jsonFile, es)

    model_name = fname.split("_")[-2:][0]
    if "autoBOT" in model_name:
        model_name = model_name.split("/")[-1]

    full_spec = jsonObject['model_specification']
    out_row = [
        model_name, jsonObject['accuracy'],
        jsonObject['macro avg']['precision'],
        jsonObject['macro avg']['recall'], jsonObject['macro avg']['f1-score'],
        jsonObject['macro avg']['support'], full_spec
    ]
    return out_row

def retrieve_top_model(results_folder, top_model):

    for el in glob.glob(results_folder+"/*"):
        mpath = el+"/models.tar.gz"
        core = "tar -xvzf"
        os.system(core+mpath)

    
    for el in glob.glob("./models/*"):
        if not top_model in el:
            command = "rm -rf"+" "+el
            os.system(command)

if __name__ == "__main__":

    all_rows = []
    outputs_folder = "./predictions/*"

    for el in glob.glob(outputs_folder):

        try:
            rx = extract_key_rows(el)
            all_rows.append(rx)
        except:pass

    final_df = pd.DataFrame(all_rows)
    if final_df.shape[0] == 0:
        quit()
        
    final_df.columns = [
        'model', 'Accuracy', 'Macro Prec', 'Macro Rec', 'Macro F1',
        'Macro Support', 'Full model config'
    ]
    
    final_df = final_df.sort_values(by=['Accuracy'])
    print(final_df[['model','Accuracy','Macro Prec','Macro Rec']].tail())
    final_df.to_csv("./report/overall_report_classification.tsv", sep="\t")
    lrow = final_df.iloc[-1]
    
    print(lrow['Full model config'])
    model_id = lrow['Full model config']['LOCAL_JOB_ID']
    
    retrieve_top_model("./results", model_id)
    
