#########
# Main ML engine loop
#########

INTERVAL_JOBS=`cat global_config.py | grep 'GLOBAL_CONFIG_TIME' | grep -o '[0-9]'`

IFS=';' read -r -a CLUSTERS <<< "$2"
echo "Considering clusters $CLUSTERS"
echo "Starting evolution in $INTERVAL_JOBS hour segments."
SLEEPTIME=`echo "$INTERVAL_JOBS""h"`
echo "Sleep interval set to: $SLEEPTIME"

echo "Cluster assignment ........"
## Select random cluster to send to
size=${#CLUSTERS[@]}
index=$(($RANDOM % $size))
RANDOM_CLUSTER=${CLUSTERS[0]}

echo "Sending to cluster $RANDOM_CLUSTER ........."
## Establish proxy
arcproxy -S ${1:-"gen.vo.sling.si"};

echo "Proxy ........"
## Generate + send jobs

echo "Generating jobs ........"
singularity exec -B /etc/grid-security/certificates containers/autobot.sif python3 generate_jobs.py --copy_data;

echo "Sleeping for 5m just in case ........."
sleep 5m; ## dCache can have some latency

## arcsub
echo "Submitting jobs ........"
cd cluster_xrsl;ls;
NAME=$( tr -dc A-Za-z0-9 </dev/urandom | head -c 13 );
echo "arcsub -c $RANDOM_CLUSTER *.xrsl";
arcsub -c $RANDOM_CLUSTER *.xrsl -o ../current_jobs/$NAME.txt;
cd ..;

echo "Checking jobs ........"
cd results;
## Check + gather jobs
for j in ../current_jobs/*;
do
    arcstat -i $j;
    arcget -i $j;
done
cd ..;

echo "Gathering jobs + cleanup of old jobs ........"
## Check + gather predictions
singularity exec containers/autobot.sif python3 gather_predictions.py
singularity exec containers/autobot.sif python3 create_report.py
singularity exec containers/autobot.sif python3 remove_redundant_logs.py
echo "Cleaning complete. Storing the best model."

cp -rf ./models/* ./final_models/;
rm -rf ./models/*;


while sleep $SLEEPTIME;
do

    echo "Cluster assignment ........"
    ## Select random cluster to send to
    size=${#CLUSTERS[@]}
    index=$(($RANDOM % $size))
    RANDOM_CLUSTER=${CLUSTERS[0]}
    
    echo "Sending to cluster $RANDOM_CLUSTER ........."
    ## Establish proxy
    arcproxy -S ${1:-"gen.vo.sling.si"};

    echo "Proxy ........"
    ## Generate + send jobs

    echo "Generating jobs ........"
    singularity exec -B /etc/grid-security/certificates containers/autobot.sif python3 generate_jobs.py --copy_data; ## to run this in singularity, somehow bind .arc

    echo "Sleeping for 5m just in case ........."
    sleep 5m; ## dCache can have some latency

    ## arcsub
    echo "Submitting jobs ........"
    cd cluster_xrsl;ls;
    NAME=$( tr -dc A-Za-z0-9 </dev/urandom | head -c 13 );
    echo "arcsub -c $RANDOM_CLUSTER *.xrsl";
    arcsub -c $RANDOM_CLUSTER *.xrsl -o ../current_jobs/$NAME.txt;
    cd ..;

    echo "Checking jobs ........"
    cd results;
    ## Check + gather jobs
    for j in ../current_jobs/*;
    do
	arcstat -i $j;
	arcget -i $j;
    done
    cd ..;

    echo "Gathering jobs + cleanup of old jobs ........"
    ## Check + gather predictions
    singularity exec containers/autobot.sif python3 gather_predictions.py
    singularity exec containers/autobot.sif python3 create_report.py
    singularity exec containers/autobot.sif python3 remove_redundant_logs.py
    echo "Cleaning complete. Storing the best model."
    
    cp -rf ./models/* ./final_models/;
    rm -rf ./models/*;
    
done
