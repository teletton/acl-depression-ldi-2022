### generate jobs for the execution
from global_config import GLOBAL_JSON_CONFIG, data
import os
import random
from cluster_utils import clean, generate_xrsl_jobs, split_jobs, write_all_jobs
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--num",
                        default=GLOBAL_JSON_CONFIG['GLOBAL_CONFIG_NUM'],
                        type=int)
    parser.add_argument('--copy_data', dest='copy_data', action='store_true')
    parser.add_argument('--cache', dest='cache', action='store_true')
    parser.add_argument("--bsize",
                        default=GLOBAL_JSON_CONFIG['GLOBAL_CONFIG_BSIZE'],
                        type=int)
    args = parser.parse_args()

    clean(["raw_jobs", "cluster_xrsl", "cluster_pack.zip"])

    all_jobs = []

    for k in range(args.num):
        job_template = f"unzip cluster_pack.zip;ls; mkdir predictions; mkdir models; mkdir report; touch report/initfile.txt; sleep 2m;singularity exec autobot.sif python3 autobot_run.py --seed {k}; tar -zcvf models.tar.gz models; tar -zcvf predictions.tar.gz predictions;"
        all_jobs.append(job_template)

    random.shuffle(all_jobs)

    # Write to raw_jobs
    write_all_jobs(all_jobs, raw_job_dir="raw_jobs")

    # Split to minibatches
    split_jobs(folder="raw_jobs", bsize=args.bsize)

    # Use the cached Singularity image?
    if args.cache:
        data = data.replace("(cache=no)", "(cache=yes)")

    # Generate xrsl files.
    generate_xrsl_jobs(data,
                       raw_job_folder="raw_jobs",
                       cluster_xrsl_folder="cluster_xrsl")

    # Compress the source
    mcp = """
    zip -r cluster_pack data autobot_run.py nltk_data cluster_utils.py global_config.py memory
    """
    os.system(mcp)

    # Specify cache files  -  cache1 should be updated only if needed!

    # Copy to the grid's cache
    print("Copy to cluster:", args.copy_data)
    if args.copy_data:
        for el in [GLOBAL_JSON_CONFIG['GLOBAL_CONFIG_FILES']]:
            os.system(el)
