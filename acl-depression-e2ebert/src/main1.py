from json import load
from numpy.random.mtrand import random
import pandas as pd
import numpy as np
from nlp import Dataset
import tensorflow as tf
from sklearn.utils import shuffle
from transformers import TFAutoModelForSequenceClassification
from transformers import AutoTokenizer
from datasets import load_dataset, load_metric
from transformers import TrainingArguments
from transformers import Trainer
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments
from transformers import DataCollatorWithPadding
from transformers.file_utils import PaddingStrategy
from transformers.tokenization_utils_base import TruncationStrategy
from sklearn.model_selection import train_test_split


tokenizer = AutoTokenizer.from_pretrained("distilbert-base-cased")
def preprocess_function(examples):
    return tokenizer(examples["text"],truncation=True)

def model_init():
    return AutoModelForSequenceClassification.from_pretrained(
        'bert-base-cased', num_labels=3, return_dict=True)

def compute_metrics(eval_pred):
    predictions, labels = eval_pred
    #print(predictions)
    predictions = predictions.argmax(axis=-1)
    #print(predictions)
    return metric.compute(predictions=predictions, references=labels)

    
# Data
train_df = pd.read_csv("./data/train.tsv", "\t")



for i in range(len(train_df["PID"])):
    str = train_df["PID"][i]
    j = len(str) - 1
    br = ""
    while str[j] >= '0' and str[j] <= '9' and j >= 0:
        br += str[j]
        j-=1
    br = br[::-1]
    train_df["PID"][i] = int(br)

train_df =  shuffle(train_df)

for i in range(len(train_df["label"])):
    if train_df["label"][i] == "severe":
        train_df["label"][i] = int(2)
    elif train_df["label"][i] == "moderate":
        train_df["label"][i] = int(1)
    else:
        train_df["label"][i] = int(0)

eval_df = train_df[7500:]
#train_df = train_df[:7500]
small_train_df = train_df

test_df = pd.read_csv("./data/test.tsv", "\t")

for i in range(len(test_df["label"])):
    if test_df["label"][i] == "severe":
        test_df["label"][i] = int(2)
    elif test_df["label"][i] == "moderate":
        test_df["label"][i] = int(1)
    else:
        test_df["label"][i] = int(0)

test_df =  shuffle(test_df)
small_test_df = test_df




# Shuffle the data


small_test_df =  shuffle(small_test_df)

small_train_df = small_train_df.rename(columns={"text_a" : "text"})
small_test_df = small_test_df.rename(columns={"text_a" : "text"})
eval_df = eval_df.rename(columns={"text_a":"text"})

small_train_ds = Dataset.from_pandas(small_train_df[["PID", "text", "label"]])
eval_ds = Dataset.from_pandas(eval_df[["PID", "text", "label"]])
small_test_ds = Dataset.from_pandas(small_test_df[["PID", "text", "label"]])

tokenized_train = small_train_ds.map(preprocess_function)
tokenized_test = small_test_ds.map(preprocess_function)
tokenized_eval = eval_ds.map(preprocess_function)

tokenized_train.drop('text')
tokenized_train.drop('PID')
tokenized_train.drop('__index_level_0__')

tokenized_test.drop('text')
tokenized_test.drop('PID')
tokenized_test.drop('__index_level_0__')


tokenized_eval.drop('text')
tokenized_eval.drop('PID')
tokenized_eval.drop('__index_level_0__')

data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

model = AutoModelForSequenceClassification.from_pretrained("distilbert-base-cased", num_labels=3)
training_args = TrainingArguments(

    output_dir='./results',
    per_device_train_batch_size=32,
    num_train_epochs=10,
)
# Trainer object initialization
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=tokenized_train,
    tokenizer=tokenizer,
    data_collator=data_collator,
)

"""trainer.hyperparameter_search(
    direction="maximize",
    backend="ray",
    n_trials=10
)"""


from datasets import load_metric
from collections import Counter

trainer.train()
num = int(random() * 10000)
trainer.save_model(f"./models/{num}.pickle")

pred = trainer.predict(tokenized_test)
model_predictions = np.argmax(pred[0], axis=1)
print(pred)
print(pred[0])

metric = load_metric("accuracy")
metric_f1 = load_metric("f1")

final_score = metric.compute(predictions=model_predictions, references=tokenized_test['label'])
print(f"Final test accuracy: {final_score}")
final_score = metric_f1.compute(predictions=model_predictions, references=tokenized_test['label'], average="macro")

print(f"Final test f1-macro: {final_score}")

final_score = metric_f1.compute(predictions=model_predictions, references=tokenized_test['label'], average="micro")

print(f"Final test f1-micro: {final_score}")


train_labels = tokenized_test['label']
counter_labels_train = Counter(train_labels)
most_frequent_label = counter_labels_train.most_common(1)[0][0]


model_predictions = np.repeat(most_frequent_label, len(model_predictions))
majority_score = metric.compute(predictions=model_predictions, references=tokenized_test['label'])
print(f"Majority classifier (label=3): {majority_score}")